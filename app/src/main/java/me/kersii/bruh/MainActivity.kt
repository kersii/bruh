package me.kersii.bruh

import android.app.Activity
import android.media.MediaPlayer
import android.os.Bundle
import android.view.Window
import android.widget.Button

class MainActivity : Activity() {
	private var mediaPlayer: MediaPlayer? = null

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		requestWindowFeature(Window.FEATURE_NO_TITLE)
		setContentView(R.layout.activity_main)

		mediaPlayer = MediaPlayer.create(this, R.raw.bruh)
		val button: Button = findViewById(R.id.button)
		button.setOnClickListener {
			bruh(button)
		}
	}

	fun bruh(btn: Button) {
		val mp = mediaPlayer
		if (mp != null) {
			// make button spammable.
			if (mp.isPlaying) {
				mp.pause()
				mp.seekTo(0)
			}

			mp.start()
		}
	}

	override fun onStop() {
		super.onStop()

		mediaPlayer?.release()
		mediaPlayer = null
	}
}
